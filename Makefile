.PHONY: build upload monitor run config vendor help

PRINTF_FORMAT := "\033[32m%-10s\033[33m %s\033[33m %s\033[0m\n"

BOARD := "library"

all: run

build:
	pio run -e $(BOARD)

upload:
	pio run -t upload -e $(BOARD)

monitor:
	pio device monitor -e $(BOARD)

run:
	pio run -t upload -t monitor -e $(BOARD)

config:
	pio run -t menuconfig

vendor:
	pio pkg update

clean:
	rm -rf .pio

help: ## Display available commands
	@grep -E '^[%a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'