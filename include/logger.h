#ifndef ESP32_LAMP_LOGGER_H
#define ESP32_LAMP_LOGGER_H

#include <cstdio>
#include <regex>
#include <cstdarg>

#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

namespace socl {

  enum class LoggerLevel {
	  NONE, ERROR, WARN, INFO, DEBUG
  };

  class Logger {
	public:
	  explicit Logger(std::string tag);
	  Logger Empty();
	  void Debug(const char *format, ...);
	  void Debug(esp_err_t err, const char *format, ...);
	  void Notice(const char *format, ...);
	  void Notice(esp_err_t err, const char *format, ...);
	  void Error(const char *format, ...);
	  void Error(esp_err_t err, const char *format, ...);
	  void Info(const char *format, ...);
	  void Info(esp_err_t err, const char *format, ...);
	  void Warning(const char *format, ...);
	  void Warning(esp_err_t err, const char *format, ...);
	  static void ProjectLevel(LoggerLevel logger_level);
	private:
	  SemaphoreHandle_t mutex_ = nullptr;
	  std::string tag_;
	  static inline LoggerLevel logger_level_ = LoggerLevel::NONE;
	  static void PrintResult(esp_err_t err);
	  static std::string ReplaceModifiers(const std::string &format);
	  static const char *SystemTimestamp();
	  void Lock();
	  void Unlock();
  };

} //namespace socl

#endif //ESP32_LAMP_LOGGER_H
