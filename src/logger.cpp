#include "logger.h"

#include <utility>

namespace socl {

  Logger::Logger(std::string tag) {
	mutex_ = xSemaphoreCreateMutex();
	tag_ = std::move(tag);
  }

  void Logger::Debug(esp_err_t err, const char *format, ...) {
	if (logger_level_ < LoggerLevel::DEBUG) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;33m[D] %s :: %s\033[0m", SystemTimestamp(), tag_.c_str(), string);
	PrintResult(err);
	free(string);
	va_end(args);
  }

  void Logger::Debug(const char *format, ...) {
	if (logger_level_ < LoggerLevel::DEBUG) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;33m[D] %s :: %s\033[0m \n", SystemTimestamp(), tag_.c_str(), string);
	free(string);
	va_end(args);
  }

  void Logger::Notice(esp_err_t err, const char *format, ...) {
	if (logger_level_ < LoggerLevel::INFO) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;36m[N] %s :: %s\033[0m", SystemTimestamp(), tag_.c_str(), string);
	PrintResult(err);
	free(string);
	va_end(args);
  }

  void Logger::Notice(const char *format, ...) {
	if (logger_level_ < LoggerLevel::INFO) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;36m[N] %s :: %s\033[0m \n", SystemTimestamp(), tag_.c_str(), string);
	free(string);
	va_end(args);
  }

  void Logger::Error(esp_err_t err, const char *format, ...) {
	if (logger_level_ < LoggerLevel::ERROR) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;31m[E] %s :: %s\033[0m", SystemTimestamp(), tag_.c_str(), string);
	PrintResult(err);
	free(string);
	va_end(args);
  }

  void Logger::Error(const char *format, ...) {
	if (logger_level_ < LoggerLevel::ERROR) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;31m[E] %s :: %s\033[0m \n", SystemTimestamp(), tag_.c_str(), string);
	free(string);
	va_end(args);
  }

  void Logger::Info(esp_err_t err, const char *format, ...) {
	if (logger_level_ < LoggerLevel::INFO) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;34m[I] %s :: %s\033[0m", SystemTimestamp(), tag_.c_str(), string);
	PrintResult(err);
	free(string);
	va_end(args);
  }

  void Logger::Info(const char *format, ...) {
	if (logger_level_ < LoggerLevel::INFO) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;34m[I] %s :: %s\033[0m \n", SystemTimestamp(), tag_.c_str(), string);
	free(string);
	va_end(args);
  }

  void Logger::Warning(esp_err_t err, const char *format, ...) {
	if (logger_level_ < LoggerLevel::WARN) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;35m[W] %s :: %s\033[0m", SystemTimestamp(), tag_.c_str(), string);
	PrintResult(err);
	free(string);
	va_end(args);
  }

  void Logger::Warning(const char *format, ...) {
	if (logger_level_ < LoggerLevel::WARN) {
	  return;
	}
	char *string;
	va_list args;
	Lock();
	std::string s = ReplaceModifiers(format);
	format = s.c_str();
	va_start(args, format);
	vasprintf(&string, format, args);
	Unlock();
	printf("\033[0;38m%s\033[0m \033[0;35m[W] %s :: %s\033[0m \n", SystemTimestamp(), tag_.c_str(), string);
	free(string);
	va_end(args);
  }

  Logger Logger::Empty() {
	if (logger_level_ > LoggerLevel::NONE) {
	  printf("%s", "\r\n");
	}
	return *this;
  }

  void Logger::Lock() {
	xSemaphoreTake(mutex_, portMAX_DELAY);
  }

  void Logger::Unlock() {
	xSemaphoreGive(mutex_);
  }

  void Logger::PrintResult(esp_err_t err) {
	if (err != ESP_OK) {
	  printf(" \033[0;31m :: FAILED (%s)\033[0m%s", esp_err_to_name(err), "\n");
	  return;
	}
	printf(" \033[0;32m :: OK\033[0m%s", "\n");
  }

  std::string Logger::ReplaceModifiers(const std::string &format) {
	std::string r = std::regex_replace(format, std::regex("%q"), "\"%s\"");
	return r;
  }

  const char *Logger::SystemTimestamp() {
	static _LOCK_T bufferLock = nullptr;
	static char buffer[18] = {0};
	struct tm time_info{};

	time_t now = time(nullptr);
	localtime_r(&now, &time_info);

	if (bufferLock == nullptr) __lock_init(bufferLock);
	__lock_acquire(bufferLock);
	snprintf(buffer, sizeof(buffer), "%02d:%02d:%02d", time_info.tm_hour, time_info.tm_min, time_info.tm_sec);
	__lock_release(bufferLock);

	return buffer;
  }

  void Logger::ProjectLevel(LoggerLevel logger_level) {
	logger_level_ = logger_level;
  }

} //namespace socl