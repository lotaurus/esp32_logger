#include "freertos/FreeRTOS.h"

#include "logger.h"

extern "C" { void app_main(void); }

Logger *ptr_log;

void print_to_log() {
  ptr_log->Debug("test");
}

void NewLogger() {
  std::string tag = "EXAMPLE_PTR";
  ptr_log = new Logger(tag);
}

void app_main() {
  vTaskDelay(pdMS_TO_TICKS(1000));

  //Setup project logger level
  Logger::ProjectLevel(LoggerLevel::DEBUG);

  auto logger = Logger("EXAMPLE");
  logger.Empty();

  logger.Info("test one %s", "info");
  logger.Info("test two %s %s", "info", "info2");
  logger.Info("test three %q %s %q %d", "info", "info2", "info3", 1);
  logger.Info(ESP_OK, "test four %q %s %q %d", "info", "info2", "info3", 1);
  logger.Info(ESP_ERR_FLASH_BASE, "test four %q %s %q %d", "info", "info2", "info3", 1);

  logger.Warning("test one %s", "warning");
  logger.Warning("test two %s %s", "warning", "warning2");
  logger.Warning("test three %q %s %q %d", "warning", "warning2", "warning3", 2);
  logger.Warning(ESP_OK, "test four %q %s %q %d", "INFO", "info2", "info3", 1);
  logger.Warning(ESP_ERR_FLASH_BASE, "test four %q %s %q %d", "info", "info2", "info3", 1);

  logger.Debug("test one %s", "debug");
  logger.Debug("test two %s %s", "debug", "debug2");
  logger.Debug("test three %q %s %q %d", "debug", "debug2", "debug3", 3);
  logger.Debug(ESP_OK, "test four %q %s %q %d", "INFO", "info2", "info3", 1);
  logger.Debug(ESP_ERR_FLASH_BASE, "test four %q %s %q %d", "info", "info2", "info3", 1);

  logger.Error("test one %s", "error");
  logger.Error("test two %s %s", "error", "error2");
  logger.Error("test three %q %s %q %d", "error", "error2", "error3", 3);
  logger.Error(ESP_OK, "test four %q %s %q %d", "info", "info2", "info3", 1);
  logger.Error(ESP_ERR_FLASH_BASE, "test four %q %s %q %d", "info", "info2", "info3", 1);

  logger.Notice("test one %s", "notice");
  logger.Notice("test two %s %s", "notice", "notice2");
  logger.Notice("test three %q %s %q %d", "notice", "notice2", "notice3", 3);
  logger.Notice(ESP_OK, "test four %q %s %q %d", "info", "info2", "info3", 1);
  logger.Notice(ESP_ERR_FLASH_BASE, "test four %q %s %q %d", "info", "info2", "info3", 1);

  NewLogger();
  print_to_log();

  while (true) {
    vTaskDelay(pdMS_TO_TICKS(1000));
  }
}
